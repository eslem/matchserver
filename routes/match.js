
var express = require('express');
var router = express.Router();

const obj = {
    teamA: {
        id: 1,
        name: "Turkey",
        players: [
            { id: 1, number: 3 },
            { id: 2, number: 6 }
        ]
    },
    teamB: {
        id: 2,
        name: "Spain",
        players: [
            { id: 3, number: 4 },
            { id: 4, number: 6 }
        ]
    }
}
router.get('/', function (req, res, next) {
    res.send(obj);
});

module.exports = router;
